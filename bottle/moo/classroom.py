"""
6, Apr 2013

Example domain logic for the RESTful web service example.

This class provides basic configuration, storage, and logging.
"""

import sys
import os
import socket
import StringIO
import json

# moo 
from data.storage import Storage

#
# Room (virtual classroom -> Domain) functionality - note this is separated 
# from the RESTful implementation (bottle)
#
# TODO: only return objects/data and let moo.py handle formatting through 
# templates
#
class Room(object):
   # very limited content negotiation support - our format choices 
   # for output. This also shows _a way_ of representing enums in python
   json, xml, html, text = range(1,5)
   
   #
   # setup the configuration for our service
   #
   def __init__(self,base,conf_fn):
      self.host = socket.gethostname()
      self.base = base
      self.conf = {}
      
      # should emit a failure (file not found) message
      if os.path.exists(conf_fn):
         with open(conf_fn) as cf:
            for line in cf:
               name, var = line.partition("=")[::2]
               self.conf[name.strip()] = var.strip()
      else:
         raise Exception("configuration file not found.")

      # create storage
      self.__store = Storage()
   

   #
   # example: find data
   #
   def find(self,name):
      print '---> classroom.find:',name
      return self.__store.find(name)

   #
   # example: add data
   #
   def add(self,name,value):
      try:
         self.__store.insert(name,value)
         self.__store.names();
         return 'success'
      except:
         return 'failed'

      # TODO success|failure


   #
   # dump the configuration in the requested format. Note placing format logic
   # in the functional code is not really a good idea. However, it is here to
   # provide an example.
   #
   #
   def dump_conf(self,format):
      if format == Room.json:
         return self.__conf_as_json()
      elif format == Room.html:
         return self.__conf_as_html()
      elif format == Room.xml:
         return self.__conf_as_xml()
      elif format == Room.text:
         return self.__conf_as_text()
      else:
         return self.__conf_as_text()

   #
   # output as xml is supported through other packages. If
   # you want to add xml support look at gnosis or lxml.
   #
   def __conf_as_json(self):
      return "xml is hard"

   #
   #
   #
   def __conf_as_json(self):
      try:
         all = {}
         all["base.dir"] = self.base
         all["conf"] = self.conf
         return json.dumps(all)
      except:
         return "error: unable to return configuration"

   #
   #
   #
   def __conf_as_text(self):
      try:
        sb = StringIO.StringIO()
        sb.write("Room Configuration\n")
        sb.write("base directory = ")
        sb.write(self.base)
        sb.write("\n\n")
        sb.write("configuration:\n")
        
        for key in sorted(self.conf.iterkeys()):
           print >>sb, "%s=%s" % (key, self.conf[key])
        
        str = sb.getvalue()
        return str
      finally:
        sb.close()

      return "text"

   #
   #
   #
   def __conf_as_html(self):
      try:
        sb = StringIO.StringIO()
        sb.write("<html><body>")
        sb.write("<h1>")
        sb.write("Room Configuration")
        sb.write("</h1>")
        sb.write("<h2>Base Directory</h2>\n")
        sb.write(self.base)
        sb.write("\n\n")
        sb.write("<h2>Configuration</h2>\n")
        
        sb.write("<pre>")
        for key in sorted(self.conf.iterkeys()):
           print >>sb, "%s=%s" % (key, self.conf[key])
        sb.write("</pre>")
     
        sb.write("</body></html>")

        str = sb.getvalue()
        return str
      finally:
        sb.close()





####################### USER #######################


   #
   # function to add user
   # 
   def addUser(self, user):
      print '---> addUser'

      try:
        objId = self.__store.addUser(user)
        return {"success" : True, "id": objId}
      except:
        return {"success" : False}


   #
   # function to get user
   # 
   def getUser(self, email):
      print '---> getting User'

      try:
        user = self.__store.getUser(email)
        return {"success" : True, "user": user}
      except:
        return {"success" : False}


   #
   # function to update user
   # 
   def updateUser(self, email, user):
      print '---> updating User'

      try:
        user = self.__store.updateUser(email, user)
        return {"success" : True, "user": user}
      except:
        return {"success" : False}


   #
   # function to delete user
   # 
   def deleteUser(self, email):
      print '---> deleting User'

      try:
        self.__store.deleteUser(email)
        return {"success" : True}
      except:
        return {"success" : False}


   #
   # function to enroll course
   # 
   def enrollCourse(self, email, courseId):
      print '---> enrolling course'

      try:
        self.__store.enrollCourse(email, courseId)
        return {"success" : True}
      except:
        return {"success" : False}

   #
   # function to drop course
   # 
   def dropCourse(self, email, courseId):
      print '---> dropping course'

      try:
        self.__store.dropCourse(email, courseId)
        return {"success" : True}
      except:
        return {"success" : False}

####################### CATEGORY #######################

   #
   # function to add Category
   # 
   def addCategory(self, category):
      print '---> adding Category'

      try:
        objId = self.__store.addCategory(category)
        return {"success" : True, "id": objId}
      except:
        return {"success" : False}


   #
   # function to get Category
   # 
   def getCategory(self, id):
      print '---> getting Category'

      try:
        category = self.__store.getCategory(id)		
        return {"success" : True, "category": category}

      except:
        return {"success" : False}


   #
   # function to list Category
   # 
   def listCategory(self):
      print '---> Listing Category'

      try:
        categoryList = self.__store.listCategory()
	print categoryList		
        return {"success" : True, "category": categoryList}

      except:
        return {"success" : False}


####################### COURSE #######################


   #
   # function to add course
   # 
   def addCourse(self, course):
      print '---> adding course'

      try:
        objId = self.__store.addCourse(course)
        return {"success" : True, "id": objId}
      except:
        return {"success" : False}


   #
   # function to get course
   # 
   def getCourse(self, id):
      print '---> getting course'

      try:
        course = self.__store.getCourse(id)		
        return {"success" : True, "course": course}

      except:
        return {"success" : False}


   #
   # function to list course
   # 
   def listCourse(self):
      print '---> Listing courses'

      try:
        courseList = self.__store.listCourse()
	print courseList
		
        return {"success" : True, "course": courseList}

      except:
        return {"success" : False}

   #
   # function to update course
   # 
   def updateCourse(self, courseId, course):
      print '---> updating course'

      try:
        course = self.__store.updateCourse(courseId, course)
        return {"success" : True, "course": course}
      except:
        return {"success" : False}


   #
   # function to delete course
   # 
   def deleteCourse(self, courseId):
      print '---> deleting course'

      try:
        self.__store.deleteCourse(courseId)
        return {"success" : True}
      except:
        return {"success" : False}



####################### QUIZ #######################


   #
   # function to add quiz
   # 
   def addQuiz(self, quiz):
      print '---> adding quizzes'

      try:
        objId = self.__store.addQuiz(quiz)
        return {"success" : True, "id": objId}
      except:
        return {"success" : False}


   #
   # function to get quiz
   # 
   def getQuiz(self, id):
      print '---> getting quiz'

      try:
        quiz = self.__store.getQuiz(id)		
        return {"success" : True, "quiz": quiz}

      except:
        return {"success" : False}


   #
   # function to list quizes
   # 
   def listQuiz(self):
      print '---> Listing quizs'

      try:
        quizList = self.__store.listQuiz()
	print quizList
		
        return {"success" : True, "quiz": quizList}

      except:
        return {"success" : False}

   #
   # function to update quiz
   # 
   def updateQuiz(self, quizId, quiz):
      print '---> updating quiz'

      try:
        quiz = self.__store.updateQuiz(quizId, quiz)
        return {"success" : True, "quiz": quiz}
      except:
        return {"success" : False}


   #
   # function to delete quiz
   # 
   def deleteQuiz(self, quizId):
      print '---> deleting quiz'

      try:
        self.__store.deleteQuiz(quizId)
        return {"success" : True}
      except:
        return {"success" : False}


####################### ANNOUNCEMENT #######################


   #
   # function to add announcement
   # 
   def addAnnouncement(self, announcement):
      print '---> adding announcementzes'

      try:
        objId = self.__store.addAnnouncement(announcement)
        return {"success" : True, "id": objId}
      except:
        return {"success" : False}


   #
   # function to get announcement
   # 
   def getAnnouncement(self, id):
      print '---> getting announcement'

      try:
        announcement = self.__store.getAnnouncement(id)		
        return {"success" : True, "announcement": announcement}

      except:
        return {"success" : False}


   #
   # function to list announcementes
   # 
   def listAnnouncement(self):
      print '---> Listing announcements'

      try:
        announcementList = self.__store.listAnnouncement()
	print announcementList
		
        return {"success" : True, "announcement": announcementList}

      except:
        return {"success" : False}

   #
   # function to update announcement
   # 
   def updateAnnouncement(self, announcementId, announcement):
      print '---> updating announcement'

      try:
        announcement = self.__store.updateAnnouncement(announcementId, announcement)
        return {"success" : True, "announcement": announcement}
      except:
        return {"success" : False}


   #
   # function to delete announcement
   # 
   def deleteAnnouncement(self, announcementId):
      print '---> deleting announcement'

      try:
        self.__store.deleteAnnouncement(announcementId)
        return {"success" : True}
      except:
        return {"success" : False}


####################### DISCUSSION #######################


   #
   # function to add discussion
   # 
   def addDiscussion(self, discussion):
      print '---> adding discussionzes'

      try:
        objId = self.__store.addDiscussion(discussion)
        return {"success" : True, "id": objId}
      except:
        return {"success" : False}


   #
   # function to get discussion
   # 
   def getDiscussion(self, id):
      print '---> getting discussion'

      try:
        discussion = self.__store.getDiscussion(id)		
        return {"success" : True, "discussion": discussion}

      except:
        return {"success" : False}


   #
   # function to list discussiones
   # 
   def listDiscussion(self):
      print '---> Listing discussions'

      try:
        discussionList = self.__store.listDiscussion()
	print discussionList
		
        return {"success" : True, "discussion": discussionList}

      except:
        return {"success" : False}

   #
   # function to update discussion
   # 
   def updateDiscussion(self, discussionId, discussion):
      print '---> updating discussion'

      try:
        discussion = self.__store.updateDiscussion(discussionId, discussion)
        return {"success" : True, "discussion": discussion}
      except:
        return {"success" : False}


   #
   # function to delete discussion
   # 
   def deleteDiscussion(self, discussionId):
      print '---> deleting discussion'

      try:
        self.__store.deleteDiscussion(discussionId)
        return {"success" : True}
      except:
        return {"success" : False}


########################################################################

#
# test and demonstrate the setup
#
if __name__ == "__main__":
  if len(sys.argv) > 2:
     base = sys.argv[1]
     conf_fn = sys.argv[2]
     svc = Room(base,conf_fn)
     svc.dump_conf()
  else:
     print "usage:", sys.argv[0],"[base_dir] [conf file]"


