"""
Storage interface
"""

import time
import json
import traceback

#from pymongo import MongoClient
from bson.objectid import ObjectId
from pymongo import Connection
db = Connection('localhost', 27017)

class Storage(object):
 
   def __init__(self):
      # initialize our storage, data is a placeholder
      self.data = {}

      # for demo
      self.data['created'] = time.ctime()

   def insert(self,name,value):
      print "---> insert:",name,value
      try:
         self.data[name] = value
         return "added"
      except:
         return "error: data not added"

   def remove(self,name):
      print "---> remove:",name

   def names(self):
      print "---> names:"
      for k in self.data.iterkeys():
        print 'key:',k

   def find(self,name):
      print "---> storage.find:",name
      if name in self.data:
         rtn = self.data[name]
         print "---> storage.find: got value",rtn
         return rtn
      else:
         return None



####################### USER #######################

   def addUser(self, user):
	print "--> adding user:",user
	try:
		#client = MongoClient()
		#db = client.moocDb
		#users = db.user
		#client.moocDb['user'].save(user)
		objId = db.moocDb['user'].insert(user, safe=True)
		return str(objId)
	except:
		return "duplicate email"

   def getUser(self, email):
	print "--> getting user:",email
	try:
		user = db.moocDb['user'].find({"email": email})
		return self.serializeObjIdInCursor(user)
	except:
		return "error"

   def updateUser(self, email, user):
	print "--> updating user:",email
	try:
		if "own" in user:
			db.moocDb['user'].update({"email": email}, { "$addToSet": {"own":user["own"]} });
		elif "enrolled" in user:
			db.moocDb['user'].update({"email": email}, { "$addToSet": {"enrolled":user["enrolled"]} });
		elif "quizzes" in user: 
			db.moocDb['user'].update({"email": email}, { "$addToSet": {"quizzes":user["quizzes"]} });
			
		user = db.moocDb['user'].find({"email": email})
		print "heres"
		return self.serializeObjIdInCursor(user)
	except:
		return "error"

   def deleteUser(self, email):
	print "--> deleting user:",email
	try:
		db.moocDb['user'].remove({"email": email})
		return "success"
	except:
		return "error"

   def enrollCourse(self, email, courseId):
	print "--> enrolling course:",email, courseId
	try:
		db.moocDb['user'].update({"email": email}, { "$addToSet": {"enrolled":courseId} });
		return "success"
	except:
		return "error"

   def dropCourse(self, email, courseId):
	print "--> dropping course:",email, courseId
	try:
		db.moocDb['user'].update({"email": email}, { "$pull": {"enrolled":courseId} });
		return "success"
	except:
		return "error"


####################### CATEGORY #######################

   def addCategory(self, category):
	print "--> adding category:",category
	try:
		objId = db.moocDb['category'].insert(category, safe=True)
		return str(objId)
	except:
		return "conflict"


   def getCategory(self, id):
	print "--> getting category:",id
	try:
		id = ObjectId(str(id))
		category = db.moocDb['category'].find({"_id": id})
		return self.serializeObjIdInCursor(category)
	except:
		return "exception"

   def listCategory(self):
	print "--> Listing category:"
	try:
		categories = db.moocDb['category'].find()
		return self.serializeObjIdInCursorList(categories)
	except:
		return traceback.print_exc()

####################### COURSE #######################

   def addCourse(self, course):
	print "--> adding course:",course
	try:
		objId = db.moocDb['course'].insert(course, safe=True)
		return str(objId)
	except:
		return "conflict"


   def getCourse(self, id):
	print "--> getting course:",id
	try:
		id = ObjectId(str(id))
		course = db.moocDb['course'].find({"_id": id})
		return self.serializeObjIdInCursor(course)
	except:
		return "exception"

   def listCourse(self):
	print "--> Listing courses:"
	try:
		courses = db.moocDb['course'].find()
		return self.serializeObjIdInCursorList(courses)
	except:
		return "exception"


   def updateCourse(self, courseId, course):
	print "--> updating course:",courseId
	courseId = ObjectId(str(courseId))	
	try:
		#db.moocDb['course'].update({"_id": courseId}, { "$set": {"category":course["category"], 
		#							 "title":course["title"],
		#							 "section":course["section"],
    		#							 "dept":course["dept"],
		#							 "term": course["term"],
		#							 "year": course["year"],
    		#							 "Description":course["Description"],
		#							 "attachment": course["attachment"],
		#							 "version": course["version"],
		#							 "instructor": course["instructor"],
		#							 "days": course["days"],
		#							 "hours": course["hours"]
		#							}
		#					      }
		#			  );

		db.moocDb['course'].update({"_id": courseId},course)
		course = db.moocDb['course'].find({"_id": courseId})
		return self.serializeObjIdInCursor(course)
	except:
		return traceback.print_exc()

   def deleteCourse(self, courseId):
	print "--> deleting course:",courseId
	courseId = ObjectId(str(courseId))
	try:
		db.moocDb['course'].remove({"_id": courseId})
		return "success"
	except:
		return "error"


####################### QUIZ #######################

   def addQuiz(self, quiz):
	print "--> adding quiz:",quiz
	try:
		objId = db.moocDb['quiz'].insert(quiz, safe=True)
		return str(objId)
	except:
		return "conflict"


   def getQuiz(self, id):
	print "--> getting quiz:",id
	try:
		id = ObjectId(str(id))
		quiz = db.moocDb['quiz'].find({"_id": id})
		return self.serializeObjIdInCursor(quiz)
	except:
		return "exception"

   def listQuiz(self):
	print "--> Listing quizs:"
	try:
		quizes = db.moocDb['quiz'].find()
		return self.serializeObjIdInCursorList(quizes)
	except:
		return "exception"


   def updateQuiz(self, quizId, quiz):
	print "--> updating quiz:",quizId
	quizId = ObjectId(str(quizId))	
	try:
		db.moocDb['quiz'].update({"_id": quizId}, quiz)
		
		quiz = db.moocDb['quiz'].find({"_id": quizId})
		return self.serializeObjIdInCursor(quiz)
	except:
		return traceback.print_exc()

   def deleteQuiz(self, quizId):
	print "--> deleting quiz:",quizId
	quizId = ObjectId(str(quizId))
	try:
		db.moocDb['quiz'].remove({"_id": quizId})
		return "success"
	except:
		return "error"


####################### ANNOUNCEMENT #######################

   def addAnnouncement(self, announcement):
	print "--> adding announcement:",announcement
	try:
		objId = db.moocDb['announcement'].insert(announcement, safe=True)
		return str(objId)
	except:
		return "conflict"


   def getAnnouncement(self, id):
	print "--> getting announcement:",id
	try:
		id = ObjectId(str(id))
		announcement = db.moocDb['announcement'].find({"_id": id})
		return self.serializeObjIdInCursor(announcement)
	except:
		return "exception"

   def listAnnouncement(self):
	print "--> Listing announcements:"
	try:
		announcements = db.moocDb['announcement'].find()
		return self.serializeObjIdInCursorList(announcements)
	except:
		return "exception"


   def updateAnnouncement(self, announcementId, announcement):
	print "--> updating announcement:",announcementId
	announcementId = ObjectId(str(announcementId))	
	try:
		db.moocDb['announcement'].update({"_id": announcementId}, announcement)
		
		announcement = db.moocDb['announcement'].find({"_id": announcementId})
		return self.serializeObjIdInCursor(announcement)
	except:
		return traceback.print_exc()

   def deleteAnnouncement(self, announcementId):
	print "--> deleting announcement:",announcementId
	announcementId = ObjectId(str(announcementId))
	try:
		db.moocDb['announcement'].remove({"_id": announcementId})
		return "success"
	except:
		return "error"


####################### DISCUSSION #######################

   def addDiscussion(self, discussion):
	print "--> adding discussion:",discussion
	try:
		objId = db.moocDb['discussion'].insert(discussion, safe=True)
		return str(objId)
	except:
		return "conflict"


   def getDiscussion(self, id):
	print "--> getting discussion:",id
	try:
		id = ObjectId(str(id))
		discussion = db.moocDb['discussion'].find({"_id": id})
		return self.serializeObjIdInCursor(discussion)
	except:
		return "exception"

   def listDiscussion(self):
	print "--> Listing discussions:"
	try:
		discussions = db.moocDb['discussion'].find()
		return self.serializeObjIdInCursorList(discussions)
	except:
		return "exception"


   def updateDiscussion(self, discussionId, discussion):
	print "--> updating discussion:",discussionId
	discussionId = ObjectId(str(discussionId))	
	try:
		db.moocDb['discussion'].update({"_id": discussionId}, discussion)
		
		discussion = db.moocDb['discussion'].find({"_id": discussionId})
		return self.serializeObjIdInCursor(discussion)
	except:
		return traceback.print_exc()

   def deleteDiscussion(self, discussionId):
	print "--> deleting discussion:",discussionId
	discussionId = ObjectId(str(discussionId))
	try:
		db.moocDb['discussion'].remove({"_id": discussionId})
		return "success"
	except:
		return "error"


#################HELPER FUNCTIONS###################

   def serializeObjIdInCursor(self, cursor):
      serializedJson = ""
      for element in cursor:
	element["_id"] = str(element["_id"])
	serializedJson = element
      return serializedJson

   def serializeObjIdInCursorList(self, cursor):
      serializedJson = []
      for element in cursor:
	element["_id"] = str(element["_id"])
	serializedJson.append(element)
      return serializedJson
