"""
6, Apr 2013

Example bottle (python) RESTful web service.

This example provides a basic setup of a RESTful service

Notes
1. example should perform better content negotiation. A solution is
   to use minerender (https://github.com/martinblech/mimerender)
"""

import time
import sys
import socket
import json

# bottle framework
from bottle import request, response, route, run, template

# moo
from classroom import Room

# virtual classroom implementation
room = None

def setup(base,conf_fn):
   print '\n**** service initialization ****\n'
   global room 
   room = Room(base,conf_fn)

#
# setup the configuration for our service
@route('/')
def root():
   print "--> root"
   return 'welcome sid'

#
#
@route('/moo/ping', method='GET')
def ping():
   return 'ping %s - %s' % (socket.gethostname(),time.ctime())

#
# Development only: echo the configuration of the virtual classroom.
#
# Testing using curl:
# curl -i -H "Accept: application/json" http://localhost:8080/moo/conf
#
# WARN: This method should be disabled or password protected - dev only!
#
@route('/moo/conf', method='GET')
def conf():
   fmt = __format(request)
   response.content_type = __response_format(fmt)
   return room.dump_conf(fmt)

#
# example of a RESTful method. This example is very basic, it does not 
# support much in the way of content negotiation.
#
@route('/moo/echo/:msg')
def echo(msg):
   fmt = __format(request)
   response.content_type = __response_format(fmt)
   if fmt == Room.html:
      return '<h1>%s</h1>' % msg
   elif fmt == Room.json:
      rsp = {}
      rsp["msg"] = msg
      return json.dumps(all)
   else:
      return msg


#
# example of a RESTful query
#
@route('/moo/data/:name', method='GET')
def find(name):
   print '---> moo.find:',name
   return room.find(name)

#
# example adding data using forms
#
@route('/moo/data', method='POST')
def add():
   print '---> moo.add'

   # example list form values
   for k,v in request.forms.allitems():
      print "form:",k,"=",v

   name = request.forms.get('name')
   value = request.forms.get('value')
   return room.add(name,value)

#
def convert(input):
    if isinstance(input, dict):
        return {convert(key): convert(value) for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [convert(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

#
# function to add user in db
#
@route('/moo/adduser', method='POST')
def addUser():
	print '---> moo.addUser'
	data = request.body.readline()	
	if not data:
		abort(400, 'No data received')
	user = json.load(request.body)
	print user
	if not user.has_key('_id'):
		abort(400, 'No _id specified')
	return room.addUser(convert(user))



#
# Determine the format to return data (does not support images)
#
# TODO method for Accept-Charset, Accept-Language, Accept-Encoding, 
# Accept-Datetime, etc should also exist
#
def __format(request):
   #for key in sorted(request.headers.iterkeys()):
   #   print "%s=%s" % (key, request.headers[key])

   types = request.headers.get("Accept",'')
   subtypes = types.split(",")
   for st in subtypes:
      sst = st.split(';')
      if sst[0] == "text/html":
         return Room.html
      elif sst[0] == "text/plain":
         return Room.text
      elif sst[0] == "application/json":
         return Room.json
      elif sst[0] == "*/*":
         return Room.json

      # TODO
      # xml: application/xhtml+xml, application/xml
      # image types: image/jpeg, etc

   # default
   return Room.html

#
# The content type on the reply
#
def __response_format(reqfmt):
      if reqfmt == Room.html:
         return "text/html"
      elif reqfmt == Room.text:
         return "text/plain"
      elif reqfmt == Room.json:
         return "application/json"
      else:
         return "*/*"
         
      # TODO
      # xml: application/xhtml+xml, application/xml
      # image types: image/jpeg, etc





####################### USER #######################

#
# function to add user in db
#
@route('/user', method='POST')
def addUser():
	print '---> adding User'

	data = request.body.readline()	
	if not data:
		abort(400, 'No data received')
	user = json.load(request.body)
	
	print user

	return room.addUser(convert(user))

#
# function to get user from db
#
@route('/user/:email', method='GET')
def getUser(email):
	print '---> getting User'

	return room.getUser(email)

#
# function to update user in db
#
@route('/user/update/:email', method='PUT')
def updateUser(email):
	print '---> updating User'

	data = request.body.readline()	
	if not data:
		abort(400, 'No data received')
	user = json.load(request.body)

	return room.updateUser(email, user)

#
# function to delete user from db
#
@route('/user/:email', method='DELETE')
def deleteUser(email):
	print '---> deleting User'

	return room.deleteUser(email)


#
# function to enroll user in course
#
@route('/course/enroll', method='PUT')
def enrollCourse():
	print '---> enrolling User in course'

	data = request.body.readline()	
	if not data:
		abort(400, 'No data received')
	data = json.load(request.body)

	return room.enrollCourse(data["email"], data["courseId"])


#
# function to drop user from course
#
@route('/course/drop', method='PUT')
def dropCourse():
	print '---> dropping User from course'

	data = request.body.readline()	
	if not data:
		abort(400, 'No data received')
	data = json.load(request.body)

	return room.dropCourse(data["email"], data["courseId"])


####################### CATEGORY #######################

#
# function to add category in db
#
@route('/category', method='POST')
def addCategory():
	print '---> adding Category'

	data = request.body.readline()	
	if not data:
		abort(400, 'No data received')
	category = json.load(request.body)
	
	print category

	return room.addCategory(convert(category))


#
# function to get category from db
#
@route('/category/:id', method='GET')
def getCategory(id):
	print '---> getting Category'

	return room.getCategory(id)


#
# function to list categories in db
#
@route('/category/list', method='GET')
def listCategory():
	print '---> Listing Category'

	return room.listCategory()


####################### COURSE #######################

#
# function to add course in db
#
@route('/course', method='POST')
def addCourse():
	print '---> adding course'

	data = request.body.readline()	
	if not data:
		abort(400, 'No data received')
	course = json.load(request.body)
	
	print course

	return room.addCourse(course)


#
# function to get course from db
#
@route('/course/:id', method='GET')
def getCourse(id):
	print '---> getting course'

	return room.getCourse(id)


#
# function to list courses in db
#
@route('/course/list', method='GET')
def listCourse():
	print '---> Listing course'

	return room.listCourse()


#
# function to update course in db
#
@route('/course/update/:id', method='PUT')
def updateCourse(id):
	print '---> updating course'

	data = request.body.readline()	
	if not data:
		abort(400, 'No data received')
	course = json.load(request.body)

	return room.updateCourse(id, course)

#
# function to delete quiz from db
#
@route('/course/:id', method='DELETE')
def deleteCourse(courseId):
	print '---> deleting course'

	return room.deleteCourse(courseId)



####################### QUIZ #######################

#
# function to add quiz in db
#
@route('/quizzes', method='POST')
def addQuiz():
	print '---> adding quiz'

	data = request.body.readline()	
	if not data:
		abort(400, 'No data received')
	quiz = json.load(request.body)
	
	print quiz

	return room.addQuiz(quiz)


#
# function to get quiz from db
#
@route('/quiz/:id', method='GET')
def getQuiz(id):
	print '---> getting quiz'

	return room.getQuiz(id)


#
# function to list quizes in db
#
@route('/quiz/list', method='GET')
def listQuiz():
	print '---> Listing quiz'

	return room.listQuiz()


#
# function to update quiz in db
#
@route('/quiz/update/:id', method='PUT')
def updateQuiz(id):
	print '---> updating quiz'

	data = request.body.readline()	
	if not data:
		abort(400, 'No data received')
	quiz = json.load(request.body)

	return room.updateQuiz(id, quiz)

#
# function to delete quiz from db
#
@route('/quiz/:id', method='DELETE')
def deleteQuiz(id):
	print '---> deleting quiz'

	return room.deleteQuiz(id)


####################### ANNOUNCEMENT #######################

#
# function to add announcement in db
#
@route('/announcements', method='POST')
def addAnnouncement():
	print '---> adding announcement'

	data = request.body.readline()	
	if not data:
		abort(400, 'No data received')
	announcement = json.load(request.body)
	
	print announcement

	return room.addAnnouncement(announcement)


#
# function to get announcement from db
#
@route('/announcement/:id', method='GET')
def getAnnouncement(id):
	print '---> getting announcement'

	return room.getAnnouncement(id)


#
# function to list announcementes in db
#
@route('/announcement/list', method='GET')
def listAnnouncement():
	print '---> Listing announcements'

	return room.listAnnouncement()


#
# function to update announcement in db
#
@route('/announcement/update/:id', method='PUT')
def updateAnnouncement(id):
	print '---> updating announcement'

	data = request.body.readline()	
	if not data:
		abort(400, 'No data received')
	announcement = json.load(request.body)

	return room.updateAnnouncement(id, announcement)

#
# function to delete announcement from db
#
@route('/announcement/:id', method='DELETE')
def deleteAnnouncement(id):
	print '---> deleting announcement'

	return room.deleteAnnouncement(id)


####################### DISCUSSION #######################

#
# function to add discussion in db
#
@route('/discussions', method='POST')
def addDiscussion():
	print '---> adding discussion'

	data = request.body.readline()	
	if not data:
		abort(400, 'No data received')
	discussion = json.load(request.body)
	
	print discussion

	return room.addDiscussion(discussion)


#
# function to get discussion from db
#
@route('/discussion/:id', method='GET')
def getDiscussion(id):
	print '---> getting discussion'

	return room.getDiscussion(id)


#
# function to list discussiones in db
#
@route('/discussion/list', method='GET')
def listDiscussion():
	print '---> Listing discussions'

	return room.listDiscussion()


#
# function to update discussion in db
#
@route('/discussion/update/:id', method='PUT')
def updateDiscussion(id):
	print '---> updating discussion'

	data = request.body.readline()	
	if not data:
		abort(400, 'No data received')
	discussion = json.load(request.body)

	return room.updateDiscussion(id, discussion)

#
# function to delete discussion from db
#
@route('/discussion/:id', method='DELETE')
def deleteDiscussion(id):
	print '---> deleting discussion'

	return room.deleteDiscussion(id)


