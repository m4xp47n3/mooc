#!/bin/bash
#
# test client access to our service

echo -e "\n"
curl -X DELETE -H "Content-Type: application/json" http://localhost:8080/course/5191ed73090af71b29c2da81
# 5191ed73090af71b29c2da81 can be replaced by valid course id
echo -e "\n"
