#!/bin/bash
#
# test client access to our service

echo -e "\n"
curl -X PUT -H "Content-Type: application/json" -d '{
    "category": "categoryId",
    "title": "introduction to algebra",
    "section": 2,
    "dept": "eng",
    "term": "Spring",
    "year": 2013,
    "instructor": [
        {
            "name": "russel Doe",
            "email": "russel_doe@univ.com"
        }
    ],
    "days": [
        "Monday",
        "Wednesday",
        "Friday"
    ],
    "hours": [
        "8:00AM",
        "9:15:AM"
    ],
    "Description": "My course",
    "attachment": "PATH",
    "version": "1"
}' http://localhost:8080/course/update/5191ed73090af71b29c2da81
# 5191ed73090af71b29c2da81 can be replaced by valid course id
echo -e "\n"
