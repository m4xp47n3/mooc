#!/bin/bash
#
# test client access to our service

echo -e "\n"
curl -X POST -H "Content-Type: application/json" -d '{
    "category": "categoryId",
    "title": "introduction to algebra",
    "section": 2,
    "dept": "eng",
    "term": "Spring",
    "year": 2013,
    "instructor": [
        {
            "name": "russel Doe",
            "email": "russel_doe@univ.com"
        }
    ],
    "days": [
        "Monday",
        "Wednesday",
        "Friday"
    ],
    "hours": [
        "8:00AM",
        "9:15:AM"
    ],
    "Description": "My course",
    "attachment": "PATH",
    "version": "1"
}' http://localhost:8080/course
echo -e "\n"
