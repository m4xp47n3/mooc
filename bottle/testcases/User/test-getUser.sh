#!/bin/bash
#
# test client access to our service

echo -e "\n"
#test@google.com can be replaced by email of any created user
curl -X GET -H "Content-Type: application/json" http://localhost:8080/user/test@google.com
echo -e "\n"
