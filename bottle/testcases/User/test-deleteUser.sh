#!/bin/bash
#
# test client access to our service

echo -e "\n"
curl -X DELETE -H "Content-Type: application/json" http://localhost:8080/user/test@google.com
# test@google.com can be replaced by valid user email
echo -e "\n"
