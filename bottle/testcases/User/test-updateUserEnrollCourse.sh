#!/bin/bash
#
# test client access to our service

echo -e "\n"
curl -X PUT -H "Content-Type: application/json" -d '{"email":"test@google.com", "courseId": "courseId"}' http://localhost:8080/course/enroll
echo -e "\n"
