#!/bin/bash
#
# test client access to our service

echo -e "\n"
curl -X PUT -H "Content-Type: application/json" -d '{"own": ["course1"]}' http://localhost:8080/user/update/test@google.com
# test@google.com can be replaced by valid user email
echo -e "\n"
