#!/bin/bash
#
# test client access to our service

echo -e "\n"
curl -X POST -H "Content-Type: application/json" -d '{
    "email": "test@google.com",
    "own": [
        "course1",
        "course2"
    ],
    "enrolled": [
        "course3",
        "course4"
    ],
    "quizzes": [
        {
            "quiz": "id1",
            "grade": 5,
            "submitDate": "DATE"
        },
        {
            "quiz": "id1",
            "grade": 5,
            "submitDate": "DATE"
        }
    ]
}' http://localhost:8080/user
echo -e "\n"
