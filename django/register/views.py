# Create your views here.
from django.shortcuts import render_to_response

import json
import urllib
import urllib2

def home(request):
    #code here to redirect a request to bottle
    url = "http://localhost:8080/moo/adduser"
    post_data = {"_id":12, "name":"siddhi", "password":"123456", "email": "sid@gmail.com"}
    #post_data_encoded = urllib.urlencode(post_data)
    request_object = urllib2.Request(url, json.dumps(post_data))
    response = urllib2.urlopen(request_object)
    html_string = response.read()
    print html_string
    #code ends
    return render_to_response('register.html')
