# Create your views here.
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render_to_response
from django.template import RequestContext

import json
import urllib
import urllib2
import requests


def home(request):
	return render_to_response('addcategory.html',context_instance=RequestContext(request))


def addcategory(request):
	form = request.POST
	name = form['name']
	description = form['catdesc']
	createDate = form['catdate']
	status = 0
	print "data retrieved from form"+ name
	#code here to redirect a request to bottle
	url = "http://localhost:8080/category"
	post_data = {"name":name,"description":description,"createDate":createDate,"status":status}
	post_data_encoded = urllib.urlencode(post_data)
	request_object = urllib2.Request(url, json.dumps(post_data))
	response = urllib2.urlopen(request_object)
	html_string = response.read()
	print "Redirecting..."
	print " The id returned is " + html_string
	#code ends
	return render_to_response('addcategorysuccess.html',{'response':json.loads(html_string)},context_instance=RequestContext(request))

def listcategory(request):
	print "Requesting Data in Get"
	#code here to redirect a request to bottle
	url = "http://localhost:8080/category/list"
	response = requests.get(url)
	print json.loads(response.content)
	category = json.loads(response.content)
	categories = category["category"]
	catlist=[]
	for cat in categories:
		cat["id"]= cat["_id"]
		catlist.append(cat)
	print categories	
	#code ends
	return render_to_response('listcategories.html',{'response': catlist},context_instance=RequestContext(request))
7

