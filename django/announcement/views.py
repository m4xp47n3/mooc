# Create your views here.

from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render_to_response
from django.template import RequestContext
#from django.contrib.auth.models import User
#from registration.models import Routing

import json
import urllib
import urllib2
import requests


def home(request):
	courseList = listcourse(request)
	return render_to_response('addannouncement.html', {'response':courseList}, context_instance=RequestContext(request))


def listcourse(request):
	print "Requesting Data in Get"
	#code here to redirect a request to bottle
	url = "http://localhost:8080/course/list"
	response = requests.get(url)
	print json.loads(response.content)
	courses = json.loads(response.content)
	courses = courses["course"]
	courseList=[]
	for course in courses:
		course["id"]= course["_id"]
		courseList.append(course)
	print courses	
	#code ends
	return courseList

def addannouncement(request):
	form = request.POST
	courseId = form['courseId']
        title = form['title']
	desc = form['description']
	postDate = form['postDate']
	status = 0
	#making post data	
	post_data = { 
		      "courseId": courseId,
		      "title": title,
		      "description": desc,
		      "postDate": postDate,
		      "status": status
		    }

	print post_data

	url = "http://localhost:8080/announcements"

	request_object = urllib2.Request(url, json.dumps(post_data))
	response = urllib2.urlopen(request_object)
	html_string = response.read()
	print "Redirecting..."
	print " The id returned is " + html_string
	#code ends
	return render_to_response('addannouncementsuccess.html',{'response':json.loads(html_string)},context_instance=RequestContext(request))

def listannouncements(request):
	print "Requesting Data in Get"
	#code here to redirect a request to bottle
	url = "http://localhost:8080/announcement/list"
	response = requests.get(url)
	print response.content
	print json.loads(response.content)
	announcements = json.loads(response.content)
	announcements = announcements["announcement"]
	announcementList=[]
	for announcement in announcements:
		announcement["id"]= announcement["_id"]
		announcementList.append(announcement)
	print announcements
	request.session["announcementList"] = announcementList	
	#code ends
	return render_to_response('listannouncements.html',{'response': announcementList},context_instance=RequestContext(request))

def getannouncement(request):
	print "Requesting Data in Get"
	#code here to redirect a request to bottle
	id = 0
	url = "http://localhost:8080/announcement/"+id
	response = requests.get(url)
	print json.loads(response.content)
	announcement = json.loads(response.content)
	announcement = announcement["announcement"]
	announcement["id"]= announcement["_id"]
	print announcement	
	#code ends
	return render_to_response('viewannouncement.html',{'response': announcement},context_instance=RequestContext(request))

def updateannouncement(request):
	print "Requesting Data in Get"
	#code here to redirect a request to bottle
	id = 0
	url = "http://localhost:8080/announcement/update/"+id
	formData = request.PUT

	#making post data	
	data = { 
		      "courseId": formData['courseId'],
		      "title": form['title'],
		      "description": form['description'],
		      "postDate": form['postDate'],
		      "status": 0
		    }
	data = json.dumps(data)
	response = requests.put(url, data = data)
	print json.loads(response.content)
	announcement = json.loads(response.content)
	announcement = announcement["announcement"]
	announcement["id"]= announcement["_id"]
	print announcement	
	#code ends
	return render_to_response('viewannouncement.html',{'response': announcement},context_instance=RequestContext(request))

def deleteannouncement(request):
	print "Requesting Data in Delete"
	#code here to redirect a request to bottle
	id = request.POST['announcementId']
	print id
	url = "http://localhost:8080/announcement/"+id
	response = requests.delete(url)
	response = json.loads(response.content)
	print response
	#code ends
	
	return render_to_response('listannouncements.html', {'response': request.session["announcementList"]}, context_instance=RequestContext(request))
