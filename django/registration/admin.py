from django.contrib import admin
from registration.models import Member
from registration.models import Routing

admin.site.register(Member)
admin.site.register(Routing)
