from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext
from registration.forms import RegistrationForm, LoginForm
from registration.models import Member
from registration.models import Routing
from django.contrib.auth import authenticate, login, logout

import json
import urllib
import urllib2
import requests

def MemberRegistration(request):
	if request.user.is_authenticated():
                return HttpResponseRedirect('/')
        if request.method == 'POST':
                form = RegistrationForm(request.POST)
                if form.is_valid():
                        user = User.objects.create_user(username=form.cleaned_data['email'], password = form.cleaned_data['password'])
                        user.save()
                        member = Member(user=user, name=form.cleaned_data['name'])
                        member.save()
			#adding user through bottle's addUser
			data = {"email": form.cleaned_data['email']}
			post_data = json.dumps(data)
			print post_data
			response = requests.post("http://localhost:8080/user", data=post_data)
			print response.content
			#ends 
                        return HttpResponseRedirect('/login/')
                else:
                        return render_to_response('registration/register.html', {'form': form}, context_instance=RequestContext(request))
        else:
                ''' Showing a blank registration form '''
                form = RegistrationForm()
                context = {'form': form}
                return render_to_response('registration/register.html', context, context_instance=RequestContext(request))

def LoginRequest(request):
	request.session["fav_color"] = "blue"
        if request.user.is_authenticated():
                return HttpResponseRedirect('/')
        if request.method == 'POST':
                form = LoginForm(request.POST)
                if form.is_valid():
                        username = form.cleaned_data['username']
                        password = form.cleaned_data['password']
                        member = authenticate(username=username, password=password)
                        if member is not None:
                                login(request, member)
				####### Adding connected mooc information to session #######
				routings = Routing.objects.all()
				request.session["connectedMoocIPs"] = routings
                                return HttpResponseRedirect('/')
                        else:
                                return render_to_response('registration/login.html', {'form': form}, context_instance=RequestContext(request))
                else:
                        return render_to_response('registration/login.html', {'form': form}, context_instance=RequestContext(request))
        else:
                ''' Showing the login form '''
                form = LoginForm()
                context = {'form': form}
                return render_to_response('registration/login.html', context, context_instance=RequestContext(request))

def LogoutRequest(request):
        logout(request)
        return HttpResponseRedirect('/login/')
