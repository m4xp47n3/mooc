# Create your views here.
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.models import User
from registration.models import Routing

import json
import urllib
import urllib2
import requests


def home(request):
	catlist= listcat(request)
	return render_to_response('addcourse.html',{'response':catlist},context_instance=RequestContext(request))

def dropcourse(request):
	print "Inside Course Dropping"
	form = request.POST
	courseId = form['hiddendrop']
	email = request.user.username
	data = {"email": email , "courseId": courseId}
	response = requests.put("http://localhost:8080/course/drop",data=json.dumps(data))
	return render_to_response('dropsuccess.html',{'response': json.loads(response.content)},context_instance=RequestContext(request))

def viewcourse(request):
	print "Inside View Course"
	form = request.POST
	courseId = form['hiddenview']
	print str(courseId)
        courseList = request.session["courseList"]
	for course in courseList:
		if course["id"] == courseId:
			currentcourse = course
	
	print currentcourse
	#data = json.loads(currentcourse)
	print "Printed in View Course"
	return render_to_response('viewcourse.html',{'response': currentcourse},context_instance=RequestContext(request))

def updatecourse(request):
	print "Inside Update Course"
	form = request.POST
	courseid = form['courseid']
	catid = form['catid']
        coursetitle = form['coursetitle']
	coursesec = form['coursesec']
	coursedept= form['coursedept']
	courseterm = form['courseterm']
	year= form['year']
	instrname = "AnyName"
	instremail = "Any Email"
	monday= "Any day"
	tuesday= "Any day"
	wednesday = "Any day"
	hours = "Any hours"
	coursedesc = form['coursedesc']
	coursepath = form['coursepath']
	coursever= form['coursever']
	#making post data
	#combining instructor name and email
	instructorList = []
	instructor = {"name": instrname,"email": instremail}
	instructorList.append(instructor)
	#taking only 3 days value for now
	days=[]
	days.append(monday)
	days.append(tuesday)
	days.append(wednesday)
	#taking only 1 hour and adding 2 times
	hoursList = []
	hoursList.append(hours)
	hoursList.append(hours)
	print "Updating"
	post_data = { "category":catid,"title": coursetitle ,"section": coursesec,"dept": coursedept ,"term": courseterm ,"year": year ,"instructor": instructorList,"days": days,"hours": hoursList,"Description": coursedesc ,"attachment": coursepath ,"version": coursever}
	print post_data
	url = "http://localhost:8080/course/update/" + courseid
	posting_data = json.dumps(post_data)
	response = requests.put(url,data=posting_data)
	content = response.content
	courseoutput = json.loads(response.content)
	print courseoutput
	courses = courseoutput["course"]
	print courses
	return render_to_response('viewcourse.html',{'response': courses},context_instance=RequestContext(request))


def enrollcourse(request):
	print "Inside Course Enrollment"
	form = request.POST
	courseId = form['hiddenenroll']
	email = request.user.username
	data = {"email": email , "courseId": courseId}
	response = requests.put("http://localhost:8080/course/enroll",data=json.dumps(data))
	return render_to_response('enrollmentsuccess.html',{'response': json.loads(response.content)},context_instance=RequestContext(request))

def addcourse(request):
	form = request.POST
	catid = form['catid']
        coursetitle = form['coursetitle']
	coursesec = form['coursesec']
	coursedept= form['coursedept']
	courseterm = form['courseterm']
	year= form['year']
	instrname = form['instrname']
	instremail = form['instremail']
	monday= form['monday']
	tuesday= form['tuesday']
	wednesday = form['wednesday']
	hours = form['hours']
	coursedesc = form['coursedesc']
	coursepath = form['coursepath']
	coursever= form['coursever']
	#making post data
	#combining instructor name and email
	instructorList = []
	instructor = {"name": instrname,"email": instremail}
	instructorList.append(instructor)
	#taking only 3 days value for now
	days=[]
	days.append(monday)
	days.append(tuesday)
	days.append(wednesday)
	#taking only 1 hour and adding 2 times
	hoursList = []
	hoursList.append(hours)
	hoursList.append(hours)
	
	post_data = { "category":catid,"title": coursetitle ,"section": coursesec,"dept": coursedept ,"term": courseterm ,"year": year ,"instructor": instructorList,"days": days,"hours": hoursList,"Description": coursedesc ,"attachment": coursepath ,"version": coursever}
	print post_data
	url = "http://localhost:8080/course"
	#post_data = {"name":name,"description":description,"createDate":createDate,"status":status}
	#post_data_encoded = urllib.urlencode(post_data)
	request_object = urllib2.Request(url, json.dumps(post_data))
	response = urllib2.urlopen(request_object)
	html_string = response.read()
	print "Redirecting..."
	print " The id returned is " + html_string
	#code ends
	user = request.user.username
	owncourse = json.loads(html_string)
	ownedcourseid = owncourse["id"] 
	print {"own": ownedcourseid}
	putnew_data = {"own": ownedcourseid}
	data = json.dumps(putnew_data)
	url = "http://localhost:8080/user/update/"+ user
	print url
	newresponse = requests.put(url,data=data)
	return render_to_response('addccoursesuccess.html',{'response':json.loads(html_string)},context_instance=RequestContext(request))

def listcat(request):
	print "Requesting Data in Get"
	#code here to redirect a request to bottle
	url = "http://localhost:8080/category/list"
	response = requests.get(url)
	print json.loads(response.content)
	category = json.loads(response.content)
	categories = category["category"]
	catlist=[]
	for cat in categories:
		cat["id"]= cat["_id"]
		catlist.append(cat)
	print categories	
	#code ends
	return catlist

def listcourse(request):
	print "Requesting Data in Get"
	#code here to redirect a request to bottle
	#
	######################## CONNECTING TO OTHER MOOCS AND FETCHING COURSES ###########################
	connectedMoocIPs = request.session["connectedMoocIPs"]
	courselist=[]
	for connectedMooc in connectedMoocIPs:
		print connectedMooc.IPAddress
		url = "http://"+connectedMooc.IPAddress+":8080/course/list"
		response = requests.get(url)
		courseoutput = json.loads(response.content)
		courses = courseoutput["course"]
		
		for course in courses:
			#course["id"] = connectedMooc.TeamName + "_" + course["_id"]
			course["id"] = course["_id"]
			print course["id"]
			courselist.append(course)
	###################################################################################################
	
	request.session["courseList"] = courselist
	#url = "http://localhost:8080/course/list"
	#response = requests.get(url)
	#print json.loads(response.content)
	#courseoutput = json.loads(response.content)
	#courses = courseoutput["course"]
	#courselist=[]
	#for course in courses:
	#	course["id"]= course["_id"]
	#	courselist.append(course)
	#print courses	
	#code ends
	return render_to_response('listcourses.html',{'response': courselist},context_instance=RequestContext(request))
