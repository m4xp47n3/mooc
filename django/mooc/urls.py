from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
      #url(r'^$', 'register.views.home', name='home'),
      url(r'^mooc/category/$', 'category.views.home', name='category'),
      url(r'^mooc/category/addcategory$', 'category.views.addcategory', name='addcategory'),
      url(r'^mooc/category/listcategories$', 'category.views.listcategory', name='listcategory'),
      url(r'^mooc/courses$', 'courses.views.home', name='course'),
      url(r'^mooc/courses/addcourse$', 'courses.views.addcourse', name='addcourse'),
      url(r'^mooc/courses/listcourses$', 'courses.views.listcourse', name='listcourse'),
      url(r'^mooc/courses/enrolled$', 'courses.views.enrollcourse', name='enrolledcourse'),
      url(r'^mooc/courses/viewcourse$', 'courses.views.viewcourse', name='viewcourse'),
      url(r'^mooc/courses/updatecourse$', 'courses.views.updatecourse', name='updatecourse'),
      url(r'^mooc/courses/drop$', 'courses.views.dropcourse', name='dropcourse'),
      url(r'^mooc/announcements$', 'announcement.views.home', name='announcement'),
      url(r'^mooc/announcements/addannouncement$', 'announcement.views.addannouncement', name='addannouncement'),
      url(r'^mooc/announcements/listannouncements$', 'announcement.views.listannouncements', name='listannouncements'),
      url(r'^mooc/announcements/update$', 'announcement.views.updateannouncement', name='updateannouncement'),
      url(r'^mooc/announcements/delete$', 'announcement.views.deleteannouncement', name='deleteannouncement'),
    # url(r'^mooc/', include('mooc.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^register/$', 'registration.views.MemberRegistration'),
    url(r'^login/$', 'registration.views.LoginRequest'),
    url(r'^logout/$', 'registration.views.LogoutRequest'),
    url(r'^$', 'moocapp.views.index'),
    url(r'^(?P<slug>[\w\-]+)/$', 'moocapp.views.post'),
)
