from django.shortcuts import render, get_object_or_404
from moocapp.models import Post
 
def index(request):
    # get the blog posts that are published
    posts = Post.objects.filter(published=True)
    # now return the rendered template
    my_color = request.session["fav_color"]
    return render(request, 'moocapp/index.html', {'posts': posts, 'my_color': my_color})
 
def post(request, slug):
    # get the Post object
    post = get_object_or_404(Post, slug=slug)
    # now return the rendered template
    return render(request, 'moocapp/post.html', {'post': post})


